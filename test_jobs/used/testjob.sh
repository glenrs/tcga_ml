isb-cgc-pipelines submit --pipelineName blcaxxcstagextxxclinxxinfoxgain \
  --inputs gs://cancer_data_types_benchmark_input/Final_data/cv_blca_cstage_t.tsv.gz:InputData/cv_blca_cstage_t.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_age.tsv.gz:InputData/cov_blca_age.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_gender.tsv.gz:InputData/cov_blca_gender.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_other_dx.tsv.gz:InputData/cov_blca_other_dx.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_race_ethn.tsv.gz:InputData/cov_blca_race_ethn.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_smoking.tsv.gz:InputData/cov_blca_smoking.tsv.gz \
  --outputs OutputData/blcaxxcstagextxxclinxxinfoxgain.tar.gz:gs://cancer_data_types_benchmark_output/output/blcaxxcstagextxxclinxxinfoxgain.tar.gz \
  --cmd "cd /; mkdir -p blcaxxcstagextxxclinxxinfoxgain/OutputData; UserScripts/nestedboth_montecarlo \
  /blcaxxcstagextxxclinxxinfoxgain/InputData/cv_blca_cstage_t.tsv.gz,/blcaxxcstagextxxclinxxinfoxgain/InputData/cov_blca_age.tsv.gz,/blcaxxcstagextxxclinxxinfoxgain/InputData/cov_blca_gender.tsv.gz,/blcaxxcstagextxxclinxxinfoxgain/InputData/cov_blca_other_dx.tsv.gz,/blcaxxcstagextxxclinxxinfoxgain/InputData/cov_blca_race_ethn.tsv.gz,/blcaxxcstagextxxclinxxinfoxgain/InputData/cov_blca_smoking.tsv.gz \
  blcaxxcstagextxxclinxxinfoxgain \
  100 \
  10 \
  false \
  AlgorithmScripts/FeatureSelection/arff/weka/InfoGain/default \
  5,10,50,100,500,1000 \
  AlgorithmScripts/Classification/tsv/sklearn/logistic_regression/default,AlgorithmScripts/Classification/tsv/sklearn/svm_linear/default,AlgorithmScripts/Classification/tsv/sklearn/svm_rbf/default,AlgorithmScripts/Classification/tsv/sklearn/random_forest/default,AlgorithmScripts/Classification/tsv/sklearn/knn/default \
  /blcaxxcstagextxxclinxxinfoxgain/OutputData/SelectedFeaturesFile.tsv \
  /blcaxxcstagextxxclinxxinfoxgain/OutputData/PredictionsFile.tsv \
  /blcaxxcstagextxxclinxxinfoxgain/OutputData/MetricsFile.tsv \
  /blcaxxcstagextxxclinxxinfoxgain/OutputData/NestedSelectedFeaturesFile.tsv \
  /blcaxxcstagextxxclinxxinfoxgain/OutputData/NestedSummarizedSelectedFeaturesFile.tsv \
  /blcaxxcstagextxxclinxxinfoxgain/OutputData/NestedPredictionsFile.tsv \
  /blcaxxcstagextxxclinxxinfoxgain/OutputData/NestedMetricsFile.tsv \
  /blcaxxcstagextxxclinxxinfoxgain/OutputData/NestedFeatureSelectionBenchmarkFile.tsv \
  /blcaxxcstagextxxclinxxinfoxgain/OutputData/NestedClassificationBenchmarkFile.tsv \
  /blcaxxcstagextxxclinxxinfoxgain/OutputData/LogFile.tsv; \
  cd /blcaxxcstagextxxclinxxinfoxgain/OutputData; rm -fv NestedSelectedFeaturesFile.tsv; \
  tar -cvzf blcaxxcstagextxxclinxxinfoxgain.tar.gz *.tsv" \
  --imageName index.docker.io/srp33/shinylearner:version248 \
  --diskType PERSISTENT_HDD \
  --cores 1 --mem 3 \
  --diskSize 10 \
  --logsPath gs://cancer_data_types_benchmark_output/logs/blcaxxcstagextxxclinxxinfoxgain \
  --preemptible
