isb-cgc-pipelines submit --pipelineName blcaxxcstagextxxmrnaxclinxxinfoxgain \
  --inputs gs://cancer_data_types_benchmark_input/Final_data/cv_blca_cstage_t.tsv.gz:InputData/cv_blca_cstage_t.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/mrna_blca.tsv.gz:InputData/mrna_blca.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_age.tsv.gz:InputData/cov_blca_age.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_gender.tsv.gz:InputData/cov_blca_gender.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_other_dx.tsv.gz:InputData/cov_blca_other_dx.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_race_ethn.tsv.gz:InputData/cov_blca_race_ethn.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_smoking.tsv.gz:InputData/cov_blca_smoking.tsv.gz \
  --outputs OutputData/blcaxxcstagextxxmrnaxclinxxinfoxgain.tar.gz:gs://cancer_data_types_benchmark_output/output/blcaxxcstagextxxmrnaxclinxxinfoxgain.tar.gz \
  --cmd "cd /; mkdir -p blcaxxcstagextxxmrnaxclinxxinfoxgain/OutputData; UserScripts/nestedboth_montecarlo \
  /blcaxxcstagextxxmrnaxclinxxinfoxgain/InputData/cv_blca_cstage_t.tsv.gz,/blcaxxcstagextxxmrnaxclinxxinfoxgain/InputData/mrna_blca.tsv.gz,/blcaxxcstagextxxmrnaxclinxxinfoxgain/InputData/cov_blca_age.tsv.gz,/blcaxxcstagextxxmrnaxclinxxinfoxgain/InputData/cov_blca_gender.tsv.gz,/blcaxxcstagextxxmrnaxclinxxinfoxgain/InputData/cov_blca_other_dx.tsv.gz,/blcaxxcstagextxxmrnaxclinxxinfoxgain/InputData/cov_blca_race_ethn.tsv.gz,/blcaxxcstagextxxmrnaxclinxxinfoxgain/InputData/cov_blca_smoking.tsv.gz \
  blcaxxcstagextxxmrnaxclinxxinfoxgain \
  100 \
  10 \
  false \
  AlgorithmScripts/FeatureSelection/arff/weka/InfoGain/default \
  5,10,50,100,500,1000 \
  AlgorithmScripts/Classification/tsv/sklearn/logistic_regression/default,AlgorithmScripts/Classification/tsv/sklearn/svm_linear/default,AlgorithmScripts/Classification/tsv/sklearn/svm_rbf/default,AlgorithmScripts/Classification/tsv/sklearn/random_forest/default,AlgorithmScripts/Classification/tsv/sklearn/knn/default \
  /blcaxxcstagextxxmrnaxclinxxinfoxgain/OutputData/SelectedFeaturesFile.tsv \
  /blcaxxcstagextxxmrnaxclinxxinfoxgain/OutputData/PredictionsFile.tsv \
  /blcaxxcstagextxxmrnaxclinxxinfoxgain/OutputData/MetricsFile.tsv \
  /blcaxxcstagextxxmrnaxclinxxinfoxgain/OutputData/NestedSelectedFeaturesFile.tsv \
  /blcaxxcstagextxxmrnaxclinxxinfoxgain/OutputData/NestedSummarizedSelectedFeaturesFile.tsv \
  /blcaxxcstagextxxmrnaxclinxxinfoxgain/OutputData/NestedPredictionsFile.tsv \
  /blcaxxcstagextxxmrnaxclinxxinfoxgain/OutputData/NestedMetricsFile.tsv \
  /blcaxxcstagextxxmrnaxclinxxinfoxgain/OutputData/NestedFeatureSelectionBenchmarkFile.tsv \
  /blcaxxcstagextxxmrnaxclinxxinfoxgain/OutputData/NestedClassificationBenchmarkFile.tsv \
  /blcaxxcstagextxxmrnaxclinxxinfoxgain/OutputData/LogFile.tsv; \
  cd /blcaxxcstagextxxmrnaxclinxxinfoxgain/OutputData; rm -fv NestedSelectedFeaturesFile.tsv; \
  tar -cvzf blcaxxcstagextxxmrnaxclinxxinfoxgain.tar.gz *.tsv" \
  --imageName index.docker.io/srp33/shinylearner:version248 \
  --diskType PERSISTENT_HDD \
  --cores 16 --mem 104 \
  --diskSize 500 \
  --logsPath gs://cancer_data_types_benchmark_output/logs/blcaxxcstagextxxmrnaxclinxxinfoxgain \
