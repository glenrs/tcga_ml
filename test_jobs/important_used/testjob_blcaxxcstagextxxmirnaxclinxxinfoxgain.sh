isb-cgc-pipelines submit --pipelineName blcaxxcstagextxxmirnaxclinxxinfoxgain \
  --inputs gs://cancer_data_types_benchmark_input/Final_data/cv_blca_cstage_t.tsv.gz:InputData/cv_blca_cstage_t.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/mirna_blca.tsv.gz:InputData/mirna_blca.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_age.tsv.gz:InputData/cov_blca_age.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_gender.tsv.gz:InputData/cov_blca_gender.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_other_dx.tsv.gz:InputData/cov_blca_other_dx.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_race_ethn.tsv.gz:InputData/cov_blca_race_ethn.tsv.gz,gs://cancer_data_types_benchmark_input/Final_data/cov_blca_smoking.tsv.gz:InputData/cov_blca_smoking.tsv.gz \
  --outputs OutputData/blcaxxcstagextxxmirnaxclinxxinfoxgain.tar.gz:gs://cancer_data_types_benchmark_output/output/blcaxxcstagextxxmirnaxclinxxinfoxgain.tar.gz \
  --cmd "cd /; mkdir -p blcaxxcstagextxxmirnaxclinxxinfoxgain/OutputData; UserScripts/nestedboth_montecarlo \
  /blcaxxcstagextxxmirnaxclinxxinfoxgain/InputData/cv_blca_cstage_t.tsv.gz,/blcaxxcstagextxxmirnaxclinxxinfoxgain/InputData/mirna_blca.tsv.gz,/blcaxxcstagextxxmirnaxclinxxinfoxgain/InputData/cov_blca_age.tsv.gz,/blcaxxcstagextxxmirnaxclinxxinfoxgain/InputData/cov_blca_gender.tsv.gz,/blcaxxcstagextxxmirnaxclinxxinfoxgain/InputData/cov_blca_other_dx.tsv.gz,/blcaxxcstagextxxmirnaxclinxxinfoxgain/InputData/cov_blca_race_ethn.tsv.gz,/blcaxxcstagextxxmirnaxclinxxinfoxgain/InputData/cov_blca_smoking.tsv.gz \
  blcaxxcstagextxxmirnaxclinxxinfoxgain \
  100 \
  10 \
  false \
  AlgorithmScripts/FeatureSelection/arff/weka/InfoGain/default \
  5,10,50,100,500,1000 \
  AlgorithmScripts/Classification/tsv/sklearn/logistic_regression/default,AlgorithmScripts/Classification/tsv/sklearn/svm_linear/default,AlgorithmScripts/Classification/tsv/sklearn/svm_rbf/default,AlgorithmScripts/Classification/tsv/sklearn/random_forest/default,AlgorithmScripts/Classification/tsv/sklearn/knn/default \
  /blcaxxcstagextxxmirnaxclinxxinfoxgain/OutputData/SelectedFeaturesFile.tsv \
  /blcaxxcstagextxxmirnaxclinxxinfoxgain/OutputData/PredictionsFile.tsv \
  /blcaxxcstagextxxmirnaxclinxxinfoxgain/OutputData/MetricsFile.tsv \
  /blcaxxcstagextxxmirnaxclinxxinfoxgain/OutputData/NestedSelectedFeaturesFile.tsv \
  /blcaxxcstagextxxmirnaxclinxxinfoxgain/OutputData/NestedSummarizedSelectedFeaturesFile.tsv \
  /blcaxxcstagextxxmirnaxclinxxinfoxgain/OutputData/NestedPredictionsFile.tsv \
  /blcaxxcstagextxxmirnaxclinxxinfoxgain/OutputData/NestedMetricsFile.tsv \
  /blcaxxcstagextxxmirnaxclinxxinfoxgain/OutputData/NestedFeatureSelectionBenchmarkFile.tsv \
  /blcaxxcstagextxxmirnaxclinxxinfoxgain/OutputData/NestedClassificationBenchmarkFile.tsv \
  /blcaxxcstagextxxmirnaxclinxxinfoxgain/OutputData/LogFile.tsv; \
  cd /blcaxxcstagextxxmirnaxclinxxinfoxgain/OutputData; rm -fv NestedSelectedFeaturesFile.tsv; \
  tar -cvzf blcaxxcstagextxxmirnaxclinxxinfoxgain.tar.gz *.tsv" \
  --imageName index.docker.io/srp33/shinylearner:version248 \
  --diskType PERSISTENT_HDD \
  --cores 4 --mem 26 \
  --diskSize 200 \
  --logsPath gs://cancer_data_types_benchmark_output/logs/blcaxxcstagextxxmirnaxclinxxinfoxgain
