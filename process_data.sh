#!/bin/bash

experiment="$1"
inputDataFiles="$2" # will be comma-separated list
fsAlgorithm="$3"

echo "$experiment"
echo "$inputDataFiles"
echo "$fsAlgorithm"


# 0. Check another bash script that invokes this script with test values for the three input arguments.
# 1. Check OSF to see whether the output file for this experiment already exists.
#      Use a Python script to access OSF and see whether the file is there.
#      Create a variable within Python script that is the expected output file name.
# 3. If it's not there, download the input file(s) to /mnt/data/InputData.
#      First make the data directories.
#        mkdir -p /mnt/data/InputData
#        mkdir -p /mnt/data/OutputData
#        mkdir -p /mnt/data/Temp
# 4. Print the ShinyLearner command that we will use to process the data: /UserScripts/nestedboth_montecarlo --description "$experiment" --data /mnt/data/InputData/inputFile1 --data /mnt/data/InputData/inputFile2 ... --fs-algo "$fsAlgorithm" --classif-algo /AlgorithmScripts/Classification/tsv/sklearn/logistic_regression/default,/AlgorithmScripts/Classification/tsv/sklearn/svm_linear/default,/AlgorithmScripts/Classification/tsv/sklearn/svm_rbf/default,/AlgorithmScripts/Classification/tsv/sklearn/random_forest/default,/AlgorithmScripts/Classification/tsv/sklearn/knn/default --num-features 5,10,50,100,500,1000 --outer-iterations 2 --inner-iterations 2 --output-dir /mnt/data/OutputData
# 5. [With Dr. Piccolo] Put in Docker
# 6. [With Dr. Piccolo] Run with dsub
