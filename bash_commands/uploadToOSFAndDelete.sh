#!/bin/bash

bash ~/bash_commands/other_scripts/copy_output_from_bucket.sh
outputFileList="$(python ~/bash_commands/other_scripts/uploadToOSF.py)"

for file in $outputFileList:
do
  isPosted=$(python ~/bash_commands/other_scripts/isPostedToOSF.py $file)

  if [[ "$isPosted" == "True" ]]
  then
    rm ~/output/file   #Do I need to add quotes around this file or add the dollar sign?
  fi
done
