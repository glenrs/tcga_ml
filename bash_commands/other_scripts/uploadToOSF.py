import requests
import shutil
import os
import json
import gzip

def uploadFile(filepath):
  temp_list = filepath.split('/')
  temp_size = len(temp_list)
  file_name = temp_list[temp_size - 1]
  url = 'https://files.osf.io/v1/resources/4y7nk/providers/osfstorage/5930254a594d90024f9fd327/'
  with open(filepath) as fh:
    mydata = fh.read()

  response = requests.put(url,
    data=mydata,
    auth=('grexsumsion@gmail.com', 'byu17piccolo'),
    headers={'content-type':'text/plain'},
    params={'kind': 'file', 'action': 'upload', 'name': file_name}
  )

pages = range(1,100)

for page in pages:
  url = 'https://api.osf.io/v2/nodes/4y7nk/files/osfstorage/5930254a594d90024f9fd327/?page=' + str(page)
  response = requests.get(url,
    auth=('grexsumsion@gmail.com','byu17piccolo')
  )
  json_data = json.loads(response.text)

  if 'errors' in json_data.keys():
    break
  
  allFilesInOSFResults = []
  for item in json_data['data']:
    attributes = item['attributes']
    allFilesInOSFResults.append(attributes['name'])

#SET ROOT DIRECTORY
root_dir = 'output/'
root_files = os.listdir(root_dir)

for item in root_files:
    if not item in allFilesInOSFResults:
    	uploadFile(root_dir + item)
