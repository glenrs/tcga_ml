#!/bin/bash

bash process_data.sh blcaxxcstagextxxcnvxclinxxinfoxgain cv_blca_cstage_t.tsv.gz,cnv_blca.tsv.gz,cov_blca_age.tsv.gz,cov_blca_gender.tsv.gz,cov_blca_other_dx.tsv.gz,cov_blca_race_ethn.tsv.gz,cov_blca_smoking.tsv.gz /AlgorithmScripts/FeatureSelection/arff/weka/InfoGain/default
